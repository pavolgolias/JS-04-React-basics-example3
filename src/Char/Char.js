import React from 'react';

const char = (props) => {
    const componentStyle = {
        display: 'inline-block',
        padding: '16px',
        textAlign: 'center',
        margin: '16px',
        border: '1px solid black'
    };

    return <div onClick={props.clicked} style={componentStyle}>{props.character}</div>
};


export default char;
