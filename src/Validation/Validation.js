import React from 'react';

const validation = (props) => {
    const output = props.userInputLength >= 5 ? <p>Text long enough</p> : <p>Text too short</p>;

    return (
        <div>
            {output}
        </div>
    );
};

export default validation;
